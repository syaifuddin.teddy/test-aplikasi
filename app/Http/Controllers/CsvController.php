<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Khill\Lavacharts\DataTables\DataTable;
use Khill\Lavacharts\Laravel\LavachartsFacade;
use Khill\Lavacharts\Lavacharts;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;

class CsvController extends Controller
{
    public function upload(){
        return view('csv_import');
    }

    public function upload_process(Request $request){
        $this->validate($request, [
            'file' => 'required',
        ]);

        $file = $request->file('file');

        $upload_folder = 'data_file';

        $file->storeAs($upload_folder, $file->getClientOriginalName());

        return redirect('/');
    }


    private function path_to_csv(){
        return storage_path('app/data_file/').env('CSV_NAME','unknown');
    }

    private function get_query_csv($mode='r'){
        return Reader::createFromPath($this->path_to_csv(),$mode);
    }

    private function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function read_csv(Request $request)
    {
        $data = [];
        if (File::exists($this->path_to_csv())) {
            $csv = $this->get_query_csv();

            $data = $csv->setOffset(1)->fetchAll();
            $data = $this->paginate($data);
        }

        return view('csv_table', compact('data'));
    }


    private function get_selected_record($paramval='',$paramkey='CMGUnmaskedID'){
        $readcsv    = $this->get_query_csv();
        $selected   = [];

        foreach($readcsv->fetchAssoc() as $item):
            if($item[$paramkey] === $paramval):
                $newkey = array_map('trim', array_keys($item));
                $newval = array_map('trim', $item);
                $item   = array_combine($newkey, $newval);
                $selected   = $item;
                break;
            endif;
        endforeach;

        return $selected;
    }

    public function get_csv_detail(Request $request)
    {
        $param   = $request->toArray();
        $getquery  = array_keys($param);
        $paramdetail= $getquery[0];
        $selected  = $this->get_selected_record($paramdetail);

        $roe = \Lava::DataTable();
        $roe->addStringColumn('ROE')
            ->addNumberColumn('Percent')
            ->addRow(['ROE_FV14', (int) $selected["ROE_FY14"]])
            ->addRow(['ROE_FV15', (int) $selected['ROE_FY15']]);

        \Lava::PieChart('ROE', $roe, [
            'title' => 'ROE FV14 vs FV15'
        ]);

        $revenue = \Lava::DataTable();
        $revenue->addStringColumn('RWA')
            ->addNumberColumn('Sales')
            ->addNumberColumn('Net Worth')
            ->addRow(['RWA FY14', (int) preg_replace("/[^0-9]/", "", $selected['RWAFY14']), 0])
            ->addRow(['REVENUE FY14', (int) preg_replace("/[^0-9]/", "", $selected['REVENUE_FY14']), 0])
            ->addRow(['RWA FY15', (int) preg_replace("/[^0-9]/", "", $selected['RWAFY15']), 0])
            ->addRow(['REVENUE FY15', (int) preg_replace("/[^0-9]/", "", $selected['REVENUE_FY15']), 0]);

        \Lava::ComboChart('Revenue', $revenue, [
            'title' => 'Revenue & RWA FV14 vs FV15',
            'seriesType' => 'bars',
            'series' => [
                1 => ['type' => 'line']
            ],
            'legend' => [
                'position' => 'none'
            ],
        ]);

        $eop = \Lava::DataTable();
        $eop->addStringColumn('EOP')
            ->addNumberColumn('Max Temp')
            ->addRow(['TotalLimits_EOP_FY14',  (int) preg_replace("/[^0-9]/", "", $selected['TotalLimits_EOP_FY14'])])
            ->addRow(['TotalLimits_EOP_FY15',  (int) preg_replace("/[^0-9]/", "", $selected['TotalLimits_EOP_FY15'])]);

        \Lava::LineChart('EOP', $eop, [
            'title' => 'Total Limit EOP FY14 vs FY15',
            'legend' => [
                'position' => 'none'
            ],
        ]);

        $average = \Lava::DataTable();
        $average->addStringColumn('Average')
            ->addNumberColumn('FY14')
            ->addNumberColumn('FY15')
            ->addRow(['Avg Regulatory Capital',  (int) preg_replace("/[^0-9]/", "", $selected['Company_Avg_Activity_FY14']), (int) preg_replace("/[^0-9]/", "", $selected['Company_Avg_Activity_FY15'])])
            ->addRow(['NPAT Allocation',  (int) preg_replace("/[^0-9]/", "", $selected['NPAT_AllocEq_FY14']), (int) preg_replace("/[^0-9]/", "", $selected['NPAT_AllocEq_FY15X'])])
            ->addRow(['TotalLimits EOP',  (int) preg_replace("/[^0-9]/", "", $selected['TotalLimits_EOP_FY14']), (int) preg_replace("/[^0-9]/", "", $selected['TotalLimits_EOP_FY15'])])
            ->addRow(['Deposits EOP',  (int) preg_replace("/[^0-9]/", "", $selected['Deposits_EOP_FY14']), (int) preg_replace("/[^0-9]/", "", $selected['Deposits_EOP_FY15x'])]);

        \Lava::BarChart('Average', $average, [
            'title' => 'Company Average Activity FY14 vs FY15',
            'legend' => [
                'position' => 'none'
            ],
        ]);

        return view('csv_table_detail', compact( 'roe','revenue','eop','selected','paramdetail'));
    }

    public function detail_csv_update(Request $request){
        $origindata = $this->get_query_csv()->fetchAll();
        $paramdata  = $request->all();
        $paramid    = $paramdata['_paramid'];
        $selected   = $this->get_selected_record($paramid);
        $new        = [];

        if(count($selected) != 0):
            foreach($origindata as $key => $val):
                if($val[0] == $paramid):
                    $origindata[$key][25] = $paramdata['ROE_FY14'];
                    $origindata[$key][26] = $paramdata['ROE_FY15'];
                    $origindata[$key][10] = $paramdata['REVENUE_FY14'];
                    $origindata[$key][11] = $paramdata['REVENUE_FY15'];
                    $origindata[$key][19] = $paramdata['REV_RWA_FY14'];
                    $origindata[$key][20] = $paramdata['REV_RWA_FY15'];
                    $origindata[$key][14] = $paramdata['TotalLimits_EOP_FY14'];
                    $origindata[$key][15] = $paramdata['TotalLimits_EOP_FY15'];
                    $origindata[$key][23] = $paramdata['Company_Avg_Activity_FY14'];
                    $origindata[$key][24] = $paramdata['Company_Avg_Activity_FY15'];
                    $new    = $origindata;
                    break;
                endif;
            endforeach;
        endif;

        $notif = 'Gagal Edit';
        if(count($new) != 0):
            $timestamp = time();
            $write = Writer::createFromPath($this->path_to_csv(),'w+');
            Storage::copy('data_file/companies.csv',"data_file/companies_".$timestamp.'.csv');
            $write->insertAll($new);
            $notif  = 'Sukses Edit';
        endif;

        return redirect()->route('read_csv')->with('notif',$notif);
    }
}
