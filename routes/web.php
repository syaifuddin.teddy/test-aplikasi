<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*=================== Bismillahirrahmanirrahim ===================*/
Route::get('/csv-import', 'CsvController@upload')->name('upload');
Route::post('/csv-import/proses', 'CsvController@upload_process')->name('upload_process');

Route::get('/', 'CsvController@read_csv')->name('read_csv');
Route::get('/detail', 'CsvController@get_csv_detail')->name('get_csv_detail');
Route::post('/detail/proses', 'CsvController@detail_csv_update')->name('detail_csv_update');