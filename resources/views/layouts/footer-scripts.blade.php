<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script>
  @if(request()->session()->flash('notif'))
  alert({!! request()->session()->flash('notif') !!})
  @endif
</script>